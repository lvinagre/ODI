Scenario to purge ODI sessions and its reports. It clears the history by each day divided in 4 parts (1st/2nd/3rs/4th quarter).

VARIABLES
=========
#ODIPURGELOG.keepDate => Number of days to keep log (used by #ODIPURGELOG.controlDate). Defaults to 5;  
#ODIPURGELOG.controlDate => Date until purge will be made (TODAY - #ODIPURGELOG.keepDate);  
#ODIPURGELOG.purgeDate => Refreshes from SNP_SESS_TASK_LOG to check the oldest log and purge if inside the #ODIPURGELOG.controlDate interval.

TOPOLOGY
========
ORACLE_WR => Data Server which connects to ODI Work Repository.
